
## Last changed: 2022-12-19 13:36:46 UTC
version 12.1X47-D15.4;
/* By Benny Nielsen */
system {
    host-name vSRX_3;
    /* pass = Rootpass */
    root-authentication {
        encrypted-password "$1$AieEmYzR$qg87iKhyZce4dGF2uWpyy1"; ## SECRET-DATA
    }
    services {
        ssh;
    }
}
interfaces {
    ge-0/0/1 {
        unit 0 {
            family inet {
                address 1.1.1.2/30;
            }
        }
    }
    ge-0/0/2 {
        unit 0 {
            family inet {
                address 2.2.2.1/30;
            }
        }
    }
    ge-0/0/0 {
        unit 0 {
            family inet {
                address 10.56.16.80/22;
            }
        }
    }
    ge-0/0/3 {
        unit 0 {
            family inet {
                address 192.168.1.1/24;
            }
        }
    }
}
routing-options {
    static {
        route 10.1.1.0/24 next-hop 1.1.1.1;
        route 10.2.2.0/24 next-hop 2.2.2.2;
        route 0.0.0.0/0 next-hop 10.56.16.1; /* Default Route */
    }
}
security {
    nat {
        /* NAT changes the source address of egress IP packets */
        source {
            rule-set trust-to-untrust {
                from zone trust;
                to zone untrust;
                rule rule-any-to-any {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                        /* Use egress interface source IP address */
                            interface;
                        }
                    }
                }
            }
        }
    }
    policies {
        from-zone trust to-zone trust { 
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone untrust to-zone trust { 
            policy default-deny {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
        from-zone trust to-zone untrust {
            policy internet-access {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
    }
    zones {
        security-zone trust {
            tcp-rst;
            interfaces {
                ge-0/0/1.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
                ge-0/0/2.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
                ge-0/0/3.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
            }
        }
        security-zone untrust {
            interfaces {
                ge-0/0/0.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
            }
        }
    }
}
